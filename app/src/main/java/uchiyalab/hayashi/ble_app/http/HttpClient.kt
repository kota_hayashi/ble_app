package uchiyalab.hayashi.ble_app.http

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap


/**
 * Created by kota-hayashi on 2017/11/05.
 */
class HttpClient {

    var disposable: Disposable? = null
    var map_id: Int? = null
    val apiServe by lazy {
        ApiService.create()
    }
    var robotMap: MutableMap<String, Int>? = mutableMapOf()

    init {
        Log.d("Client", "Client起動")
        //fixme サーバーからマップIDの取得
        getMapID(mapName)
        //fixme サーバーからロボット名とIDの取得
        getRobots(mapName)
        //fixme 定時連絡スレッドの立ち上げ

    }

    fun postTask(taskNum: Int, nodeName: String = "", robotName: String = "") {
        Log.d("Client", "postTask実行")
        val task = Task(taskTypeMap[taskNum], mapName, nodeName, 1)//robotMap!![robotName])
        val requiredTask = RequiredTask(task)
        val call: Call<RequiredTask>  = apiServe.postTask(requiredTask)
        call.enqueue(object : Callback<RequiredTask> {
            override fun onResponse(call: Call<RequiredTask>, response: Response<RequiredTask>) {
                //response.body() have your LoginResult fields and methods  (example you have to access error then try like this response.body().getError() )

            }

            override fun onFailure(call: Call<RequiredTask>, t: Throwable) {
                //for getting error in network put here Toast, so get the error on network
            }
        })
    }

    private fun getRobots(map_name: String) {
        Log.d("Client", "getRobot実行")
        disposable = apiServe.getRobots(map_name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            Log.d("Client",result.toString())
                            for (part in result) {
                                Log.d("Client", part.name)
                                Log.d("Client", part.id.toString())
                                robotMap!!.put(part.name, part.id)
                            }
                        },
                        { error -> Log.d("Client", "error occurred") }
                )
    }

    private fun getMapID(map_name: String) {
        Log.d("Client", "getMapID実行")
        disposable = apiServe.getMapId(map_name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            map_id = result.map_id
                            Log.d("Client", "map_id:" + map_id.toString())
                        },
                        { error -> Log.d("Client", error.message) }
                )
    }

//    override fun onBind(intent: Intent): IBinder? {
//        return null
//    }

    companion object {
        val taskTypeMap: HashMap<Int, String>
                = hashMapOf(1 to "Ordinary Contact",
                2 to "Found Request",
                3 to "Summon to")

        private val mapName: String = "Nitech2-2"
    }
}