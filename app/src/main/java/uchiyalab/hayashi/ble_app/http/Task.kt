package uchiyalab.hayashi.ble_app.http

/**
 * Created by kota-hayashi on 2017/11/07.
 * Taskをポストするための型
 * 本体
 * task_type
 * Ordinary Contact:一定間隔で行う居場所報告
 * Found Request:推定位置を元に近くに呼び出す（優先度低）
 * Summon to:ノードを指定し移動させる（優先度高）
 */
data class Task(val task_type: String?, val map_name: String = "", val node_name: String = "", val robot_id: Int? = 1)
