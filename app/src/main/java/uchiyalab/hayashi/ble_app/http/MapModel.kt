package uchiyalab.hayashi.ble_app.http

/**
 * Created by kota-hayashi on 2017/11/08.
 */
/**
 * Created by kota-hayashi on 2017/11/06.
 * 管理サーバとやり取りするマップモデルの形式
 */
object MapModel {
    data class Map(val map_id: Int)
}