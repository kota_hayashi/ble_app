package uchiyalab.hayashi.ble_app.util

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message

/**
 * Created by kota-hayashi on 2017/11/04.
 */

class UpdateReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        val bundle = intent.extras
        val message = bundle!!.getString("message")

        if (handler != null) {
            val msg = Message()

            val data = Bundle()
            data.putString("message", message)
            msg.data = data
            handler!!.sendMessage(msg)
        }
    }

    /**
     * メイン画面の表示を更新
     */
    fun registerHandler(locationUpdateHandler: Handler) {
        handler = locationUpdateHandler
    }

    companion object {

        var handler: Handler? = null
    }

}