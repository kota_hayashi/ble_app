package uchiyalab.hayashi.ble_app.ble

import android.app.Service
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.RemoteException
import android.util.Log

import org.altbeacon.beacon.Beacon
import org.altbeacon.beacon.BeaconManager
import org.altbeacon.beacon.BeaconParser
import org.altbeacon.beacon.Identifier
import org.altbeacon.beacon.RangeNotifier
import org.altbeacon.beacon.Region
import org.altbeacon.beacon.powersave.BackgroundPowerSaver
import org.altbeacon.beacon.startup.BootstrapNotifier
import org.altbeacon.beacon.startup.RegionBootstrap

import uchiyalab.hayashi.ble_app.MainActivity

import java.util.logging.Logger.global


/**
 * Created by kota-hayashi on 2017/11/04.
 */

// BeaconScannerクラス
// Serviceを拡張し、BootstrapNotifierをインターフェースとしたBeaconScannerクラス
open class BLEScannerBackGround : Service(), BootstrapNotifier {
    // BGで監視するiBeacon領域
    private var regionBootstrap: RegionBootstrap? = null
    // iBeacon検知用のマネージャー
    private var beaconManager: BeaconManager? = null
    // UUID設定用
    private val identifier: Identifier? = null
    // iBeacon領域
    private var region: Region? = null
    // 監視するiBeacon領域の名前
    private var beaconName: String? = null
    // スキャンした回数
    internal var count = 0
    // handler
    private var handler: Handler? = null


    override fun onCreate() {
        super.onCreate()

        BackgroundPowerSaver(this)

        // iBeaconのデータを受信できるようにParserを設定
        beaconManager = BeaconManager.getInstanceForApplication(this)
        beaconManager!!.beaconParsers.add(BeaconParser().setBeaconLayout(IBEACON_FORMAT))
        // BGでiBeacon領域を監視(モニタリング)するスキャン間隔を設定
        beaconManager!!.backgroundBetweenScanPeriod = 1000

        // UUIDの作成
        //        identifier = Identifier.parse("A56BA1E1-C06E-4C08-8467-DB6F5BD04486");
        // Beacon名の作成
        beaconName = "Beacon001"
        // major, minorの指定はしない
        region = Region(beaconName!!, null, null, null)
        regionBootstrap = RegionBootstrap(this, region)

        beaconManager!!.setRangeNotifier { beacons, region ->
            var log_strings = ""
            var major: Int = 0
            var rssi: Int = -123
            // 検出したビーコンの情報を全部Logに書き出す
            for (beacon in beacons) {
                val log_string = "major:" + beacon.id2 + "\nUUID:" + beacon.id1 + ", minor:" + beacon.id3 + ", Distance:" + beacon.distance + ",RSSI" + beacon.rssi + ", TxPower" + beacon.txPower
                Log.d(TAG, log_string)
                count++
                log_strings += log_string + "\n"
                val message = count.toString()
                sendBroadCast("1@" + message)
                // 取得した電波からrssiが最大のものを最寄りBeaconと推定
                if (major2nodeMap!!.containsKey(beacon.id2.toInt())) {
                    Log.d("Beacon", beacon.rssi.toString())
                    if (beacon.rssi.toInt() > rssi){
                        rssi = beacon.rssi
                        major = beacon.id2.toInt()
                    }
                }
            }
            val message2 = log_strings
            sendBroadCast("2@" + message2)
            if (major2nodeMap!!.containsKey(major)) {
                sendBroadCast("3@" + major2nodeMap[major])
            }
        }
    }


    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun didEnterRegion(region: Region) {
        // 領域侵入
        Log.d(TAG, "Enter Region")
        // アプリをFG起動させる
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)

        try {
            // レンジング開始
            beaconManager!!.startRangingBeaconsInRegion(region)
        } catch (e: RemoteException) {
            // 例外が発生した場合
            e.printStackTrace()
        }

    }

    override fun didExitRegion(region: Region) {
        // 領域退出
        Log.d(TAG, "Exit Region")
        try {
            // レンジング停止
            beaconManager!!.stopRangingBeaconsInRegion(region)
        } catch (e: RemoteException) {
            // 例外が発生した場合
            e.printStackTrace()
        }

    }

    override fun didDetermineStateForRegion(i: Int, region: Region) {
        // 領域に対する状態が変化
        Log.d(TAG, "Determine State: " + i)
    }

    fun registerHandler(UpdateHandler: Handler) {
        handler = UpdateHandler
    }

    private fun sendBroadCast(message: String) {

        val broadcastIntent = Intent()
        broadcastIntent.putExtra("message", message)
        broadcastIntent.action = "UPDATE_ACTION"
        baseContext.sendBroadcast(broadcastIntent)

    }

    companion object {

        val TAG = org.altbeacon.beacon.service.BeaconService::class.java.simpleName

        // iBeaconのデータを認識するためのParserフォーマット
        val IBEACON_FORMAT = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"

        private val PERMISSION_REQUEST_COARSE_LOCATION = 1

        // BLEのmajor値とNode番号の対応マップ
        private val major2nodeMap: HashMap<Int, String>?
                = hashMapOf(218 to "Node6",
                219 to "Node5",
                220 to "Node3",
                221 to "Node1",
                222 to "Node9",
                223 to "Node8",
                224 to "Node16",
                225 to "Node10",
                226 to "Node12",
                227 to "Node14",
                228 to "Node18",
                229 to "Node19",
                230 to "Node17",
                231 to "Node19",
                232 to "Node22",
                233 to "Node21",
                234 to "Node7"
                )
    }

}
