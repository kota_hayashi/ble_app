package uchiyalab.hayashi.ble_app.http

/**
 * Created by kota-hayashi on 2017/11/07.
 */
/**
 * Created by kota-hayashi on 2017/11/06.
 * 管理サーバとやり取りするロボットモデルの形式
 */
object RobotModel {
    data class Result(val id: Int, val name: String, val x: Float, val y: Float)
    data class MapModel(val id: Int, val name: String, val map_file: String, val last_update: String, val map_id: String, val node_id: String, val created_at: String, val updated_at: String)
}