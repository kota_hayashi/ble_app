package uchiyalab.hayashi.ble_app.http

/**
 * Created by kota-hayashi on 2017/11/06.
 */
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiService {

//    ロボットの情報を取得するためのメソッド
    @GET("robots.json")
    fun getRobots(@Query("map_name") map_name: String): Observable<ArrayList<RobotModel.Result>>


    // マップのidを取得するためのメソッド
    @GET("maps.json")
    fun getMapId(@Query("map_name") map_name: String): Observable<MapModel.Map>


    // タスクを投げるためのメソッド
    @Headers("content-type: application/json")
//    @Multipart
//    @FormUrlEncoded
    @POST("tasks.json")
    fun postTask(
            @Body task: RequiredTask
    ): Call<RequiredTask>

    companion object {
        fun create(): ApiService {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://133.68.101.68:3000/api/v1/")
                    .build()

            return retrofit.create(ApiService::class.java)
        }
    }

}