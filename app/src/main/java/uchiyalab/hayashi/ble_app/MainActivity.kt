package uchiyalab.hayashi.ble_app

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import uchiyalab.hayashi.ble_app.ble.BLEScannerBackGround
import android.widget.TextView
import android.content.IntentFilter
import android.view.View
import uchiyalab.hayashi.ble_app.http.HttpClient
import uchiyalab.hayashi.ble_app.util.UpdateReceiver

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var upReceiver: UpdateReceiver? = null
    private var intentFilter: IntentFilter? = null
    private var count_tv: TextView? = null
    private var bles_tv: TextView? = null
    private var estimatedNodeTV: TextView? = null
    private var httpClient: HttpClient? = null
    private var estimatedNode: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        // サービス起動
        // BLE
        startService(Intent(this@MainActivity, BLEScannerBackGround::class.java))
        // HTTPClient
//        startService(Intent(this@MainActivity, HttpClient::class.java))
        httpClient = HttpClient()

        // BLESccanerの結果をViewに反映するためのレシーバ等
        upReceiver = UpdateReceiver()
        intentFilter = IntentFilter()
        intentFilter!!.addAction("UPDATE_ACTION")
        registerReceiver(upReceiver, intentFilter)

        upReceiver!!.registerHandler(bleScanHandler)
        count_tv = findViewById<View>(R.id.textView3) as TextView
        bles_tv = findViewById<View>(R.id.textView2) as TextView
        estimatedNodeTV = findViewById<View>(R.id.textView) as TextView

    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        Log.d("Node", "NavigationItemSetected")
        when (item.itemId) {
            R.id.estimated -> {
                Log.d("Node", "Summon to a estimated node: " + estimatedNode)
                if (estimatedNode != null && estimatedNode == "推定ノード") {
                    httpClient!!.postTask(2, estimatedNode!!)
                }
            }
            R.id.node_1 -> {
                Log.d("Node", "Summon to a node1")
                httpClient!!.postTask(3,"Node1")
            }
            R.id.node_2 -> {
                Log.d("Node", "Summon to a node2")
                httpClient!!.postTask(3,"Node2")
            }
            R.id.node_3 -> {
                Log.d("Node", "Summon to a node3")
                httpClient!!.postTask(3,"Node3")
            }
            R.id.node_4 -> {
                Log.d("Node", "Summon to a node4")
                httpClient!!.postTask(3,"Node4")
            }
            R.id.node_5 -> {
                Log.d("Node", "Summon to a node5")
                httpClient!!.postTask(3,"Node5")
            }
            R.id.node_6 -> {
                Log.d("Node", "Summon to a node6")
                httpClient!!.postTask(3,"Node6")
            }
            R.id.node_7 -> {
                Log.d("Node", "Summon to a node7")
                httpClient!!.postTask(3,"Node7")
            }
            R.id.node_8 -> {
                Log.d("Node", "Summon to a node8")
                httpClient!!.postTask(3,"Node8")
            }
            R.id.node_9 -> {
                Log.d("Node", "Summon to a node9")
                httpClient!!.postTask(3,"Node9")
            }
            R.id.node_10 -> {
                Log.d("Node", "Summon to a node10")
                httpClient!!.postTask(3,"Node10")
            }
            R.id.node_11 -> {
                Log.d("Node", "Summon to a node11")
                httpClient!!.postTask(3,"Node11")
            }
            R.id.node_12 -> {
                Log.d("Node", "Summon to a node12")
                httpClient!!.postTask(3,"Node12")
            }
            R.id.node_13 -> {
                Log.d("Node", "Summon to a node13")
                httpClient!!.postTask(3,"Node13")
            }
            R.id.node_14 -> {
                Log.d("Node", "Summon to a node14")
                httpClient!!.postTask(3,"Node14")
            }
            R.id.node_15 -> {
                Log.d("Node", "Summon to a node15")
                httpClient!!.postTask(3,"Node15")
            }
            R.id.node_16 -> {
                Log.d("Node", "Summon to a node6")
                httpClient!!.postTask(3,"Node6")
            }
            R.id.node_17 -> {
                Log.d("Node", "Summon to a node17")
                httpClient!!.postTask(3,"Node17")
            }
            R.id.node_18 -> {
                Log.d("Node", "Summon to a node18")
                httpClient!!.postTask(3,"Node18")
            }
            R.id.node_19 -> {
                Log.d("Node", "Summon to a node19")
                httpClient!!.postTask(3,"Node19")
            }
            R.id.node_20 -> {
                Log.d("Node", "Summon to a node20")
                httpClient!!.postTask(3,"Node20")
            }
            R.id.node_21 -> {
                Log.d("Node", "Summon to a node21")
                httpClient!!.postTask(3,"Node21")
            }
            R.id.node_22 -> {
                Log.d("Node", "Summon to a node22")
                httpClient!!.postTask(3,"Node22")
            }
            
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    // BLEScannerから値を受け取るHandler
    private val bleScanHandler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {

            val bundle = msg.data
            val message = bundle.getString("message")
            val messages = message.split("@")
//            Log.d("Activityの名前", "はんどらーだよ" + messages[1])
            when (messages[0]) {
                "1" -> {
                    // 電波取得回数の表示
                    count_tv!!.text = messages[1]
                }
                "2" -> {
                    // 電波取得状況の表示
                    bles_tv!!.text = messages[1]
                }
                "3" -> {
                    // 推定ノードの表示
                    // 推定ノードの記録
                    estimatedNodeTV!!.text = messages[1]
                    estimatedNode = messages[1]
                }
            }

        }
    }
}
